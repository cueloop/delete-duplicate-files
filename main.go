/*
 * Delete files with duplicate contents within a directory
 * Recursevly deletes in subdirectories
 * 
 * 
 * Author: anders@cueloop.se Sep 2020
*/


package main

import (
	"os"
	"log"
	"io/ioutil"
	"fmt"
	"sort"
)



func main() {

	// we need a directory to work on
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s [directory]\n", os.Args[0])
		os.Exit(1)
	}

	
	deleteDuplicateFiles(os.Args[1])

}


func deleteDuplicateFiles(dir string) {

	// get fileInfos from directory contents
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}

	// sort on size and modification time
	sort.Slice(files, func(i, j int) bool {

		if files[i].Size() < files[j].Size() {
			return true
		} else if files[i].Size() > files[j].Size() {
			return false
		} else {
			return files[i].ModTime().Before(files[j].ModTime())
		}
	})

	i := 0
	var current *File

	// Find current file or dive into directory
	for ; i < len(files); i++ {

		current = newFile(files[i], dir)

		if current.IsDir() {
			deleteDuplicateFiles(current.Path)
		} else {
			i++
			break
		}
	}
	
	// Find next file or dive into directory
	for ; i < len(files); i++ {

		next := newFile(files[i], dir)

		if next.IsDir() {
			deleteDuplicateFiles(next.Path)
			continue
		}

		// Compare
		if !current.isDuplicate(next) {
			current = next
			continue
		}

		fmt.Printf("Found duplicate: %s | %s\n", current.Name(), next.Name())

		next.Delete()
		
	}
}





