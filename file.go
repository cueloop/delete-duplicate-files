/*
 * Wrapper of os.FileInfo for caching
 * and comparing the contents of files
 * with the purpose of finding duplicates
 * 
 * Author: anders@cueloop.se Sep 2020
*/

package main

import (
	"os"
	"log"
	"bytes"
	"path"
	"hash/crc32"
	"hash"
	"io"
	"fmt"
)

const (
	PEEK_SIZE = 1024
)

type File struct {
	Info os.FileInfo
	Path string
	File *os.File
	begin []byte
	end []byte
	Hash hash.Hash32
}

func newFile(info os.FileInfo, dir string) *File {
	f := File{Info: info}
	f.Path = path.Join(dir, info.Name())
	return &f
}

func (f *File) Name() string {
	return f.Info.Name()
}

func (f *File) Size() int64 {
	return f.Info.Size()
}


func (f *File) IsDir() bool {
	return f.Info.IsDir()
}


func (f *File) Begin() []byte {
	if f.begin == nil {
		f.begin = make([]byte, PEEK_SIZE)
		f.Open()
		defer f.Close()	
		f.File.Seek(0, io.SeekStart)
		f.File.Read(f.begin)
	}
	return f.begin
}

func (f *File) End() []byte {
	if f.end == nil {
		f.end = make([]byte, PEEK_SIZE)
		f.Open()
		defer f.Close()	
		f.File.Seek(PEEK_SIZE, io.SeekEnd)
		f.File.Read(f.end)
	}
	return f.end
}

func (f *File) Sum32() uint32 {
	if f.Hash == nil {
		f.Hash = crc32.NewIEEE()
		f.Open()
		defer f.Close()		
		f.File.Seek(0, io.SeekStart)
		io.Copy(f.Hash, f.File)
	}
	return f.Hash.Sum32()
}

func (f *File) Open() {
	var err error
	f.File, err = os.Open(f.Path)
	if err != nil {
		log.Fatal(err)
	}
}

func (f *File) Close() {
	f.File.Close()
}

func (f *File) Delete() {
	fmt.Printf("Deleting: %s\n", f.Path)
	// Delete file
	os.Remove(f.Path)
}


func (f *File) isDuplicate(other *File) bool {

	// Check size 
	if f.Size() != other.Size() {
		return false
	}

	// Check first 1k content
	if !bytes.Equal(f.Begin(), other.Begin()) {
		return false
	}

	// check last 1k content
	if !bytes.Equal(f.End(), other.End()) {
		return false
	}

	// Check CRC32 checksum
	return f.Sum32() == other.Sum32()
}

