# Delete duplicate files

A command line tool to find and delete files with duplicate content within a folder.